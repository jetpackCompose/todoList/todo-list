package ir.saeid.todo.helper

import android.util.Log

private const val tag = "3691"
fun tryCatch(className : String,funName : String,operation : () -> Unit)
{
    try
    {
        operation.invoke()
    }
    catch (e:Exception)
    {
        e.printStackTrace()
        Log.e(tag, "$className -> $funName -> ${e.message}", e.cause)
    }
}