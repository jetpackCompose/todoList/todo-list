package ir.saeid.todo.data.dao

import androidx.room.*
import ir.saeid.todo.data.model.entity.TaskEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface TaskDao
{
    @Query("select * from Tasks where doneState = 0 ORDER BY id ")
    fun getAll(): Flow<List<TaskEntity>>

    @Query("select * from Tasks where doneState = 1 ORDER BY id ")
    fun getAllTaskIsDone(): Flow<List<TaskEntity>>

    @Query("select * from Tasks where irDate = :date and doneState = 0 ")
    fun getByIrDate(date : String): Flow<List<TaskEntity>>

    @Query("select * from Tasks where enDate = :date and doneState = 0 ")
    fun getByEnDate(date : String): Flow<List<TaskEntity>>

    @Insert
    fun insert(installation: TaskEntity)

    @Update
    fun update(installation: TaskEntity)

    @Insert
    fun insertAll(installation: List<TaskEntity>)

    @Delete
    fun delete(installation: TaskEntity)

    @Query("delete from Tasks")
    fun deleteAll()


}