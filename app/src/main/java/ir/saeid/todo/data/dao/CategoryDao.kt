package ir.saeid.todo.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import ir.saeid.todo.data.model.entity.CategoryEntity

@Dao
interface CategoryDao
{
    @Query("select * from Category ORDER BY id ")
    fun getAll(): LiveData<List<CategoryEntity>>

    @Query("select * from Category where id = :ID ")
    fun getByID(ID:Int): CategoryEntity?

    @Insert
    fun insert(installation: CategoryEntity)

    @Update
    fun update(installation: CategoryEntity)

    @Insert
    fun insertAll(installation: List<CategoryEntity>)

    @Delete
    fun delete(installation: CategoryEntity)

    @Query("delete from Category")
    fun deleteAll()
}