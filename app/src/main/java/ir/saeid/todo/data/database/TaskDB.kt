package ir.saeid.todo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.saeid.todo.data.dao.CategoryDao
import ir.saeid.todo.data.dao.TaskDao
import ir.saeid.todo.data.model.entity.CategoryEntity
import ir.saeid.todo.data.model.entity.TaskEntity

@Database(entities = [TaskEntity::class,CategoryEntity::class], version = 1,exportSchema = false)
abstract class TaskDB: RoomDatabase()
{
    abstract fun taskDao(): TaskDao
    abstract fun categoryDao(): CategoryDao
}