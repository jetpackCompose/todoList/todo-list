package ir.saeid.todo.data.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Category")
class CategoryEntity
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Int  = 0

    @ColumnInfo(name = "title")
    var title: String  = ""

    @ColumnInfo(name = "imageRes")
    var imageRes: Int  = 0

    @ColumnInfo(name = "bgColor")
    var bgColor: String  = ""

    @ColumnInfo(name = "selected")
    var selected: Int  = 0
}