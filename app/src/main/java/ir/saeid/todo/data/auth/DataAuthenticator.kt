package ir.saeid.todo.data.auth

import ir.saeid.todo.data.model.entity.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

class DataAuthenticator
{
    fun authenticateText(data:String,minLength:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking data != "" && data.length >= minLength
        }
    }

    fun authenticateInt(data:Int,notValue:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking data != notValue
        }
    }

    fun authenticateTaskListForDone(data:List<TaskEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO){
            var selected = false
            for (item in data)
            {
                if (item.selected == 1)
                {
                    selected = true
                    break
                }
            }
            return@runBlocking selected
        }
    }
}