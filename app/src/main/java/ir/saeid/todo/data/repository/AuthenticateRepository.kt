package ir.saeid.todo.data.repository

import ir.saeid.todo.data.auth.DataAuthenticator
import ir.saeid.todo.data.model.entity.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class AuthenticateRepository @Inject constructor(private val auth: DataAuthenticator)
{
    fun authenticateText(data:String,minLength:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking auth.authenticateText(data,minLength)
        }
    }

    fun authenticateInt(data:Int,notValue:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking auth.authenticateInt(data,notValue)
        }
    }

    fun authenticateTaskListForDone(data:List<TaskEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking auth.authenticateTaskListForDone(data)
        }
    }
}