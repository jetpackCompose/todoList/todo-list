package ir.saeid.todo.data.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Tasks",
    foreignKeys = [androidx.room.ForeignKey(
    entity = CategoryEntity::class,
    parentColumns = ["id"],
    childColumns = ["categoryID"],
    onDelete = androidx.room.ForeignKey.CASCADE,
)]

)
class TaskEntity
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Int  = 0

    @ColumnInfo(name = "categoryID")
    var categoryID : Int  = 0

    @ColumnInfo(name = "title")
    var title: String  = ""

    @ColumnInfo(name = "description")
    var description: String  = ""

    @ColumnInfo(name = "irDate")
    var irDate: String  = ""

    @ColumnInfo(name = "enDate")
    var enDate: String  = ""


    @ColumnInfo(name = "irMonthName")
    var irMonthName: String  = ""

    @ColumnInfo(name = "enMonthName")
    var enMonthName: String  = ""

    @ColumnInfo(name = "irDay")
    var irDay: String  = ""

    @ColumnInfo(name = "enDay")
    var enDay: String  = ""


    @ColumnInfo(name = "time")
    var time: String  = ""

    @ColumnInfo(name = "doneState")
    var doneState : Int = 0

    @ColumnInfo(name = "selected")
    var selected : Int = 0
}