package ir.saeid.todo.data.repository

import ir.saeid.todo.App
import ir.saeid.todo.data.database.TaskDB
import ir.saeid.todo.data.model.entity.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class TaskRepository @Inject constructor(db: TaskDB)
{
    private val dao = db.taskDao()

    fun getAllData(): Flow<List<TaskEntity>>
    {
        return flow {
            emitAll( dao.getAll())
        }

    }

    fun getAllTaskIsDone(): Flow<List<TaskEntity>> = dao.getAllTaskIsDone()


    fun getByDate(date: String): Flow<List<TaskEntity>>
    {
        return if (App.appLanguage == App.ENGLISH)
            flow { emitAll(dao.getByEnDate(date)) }
        else
            flow { emitAll(dao.getByIrDate(date)) }
    }



    fun update(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.update(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insert(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.insert(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insertList(data: List<TaskEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.insertAll(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun delete(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.delete(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun deleteAll():Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.deleteAll()
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }
}

