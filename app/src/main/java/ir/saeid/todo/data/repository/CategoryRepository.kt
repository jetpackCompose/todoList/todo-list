package ir.saeid.todo.data.repository

import androidx.lifecycle.LiveData
import ir.saeid.todo.data.database.TaskDB
import ir.saeid.todo.data.model.entity.CategoryEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class CategoryRepository@Inject constructor(db: TaskDB)
{
    private val dao = db.categoryDao()

    fun getAllData(): LiveData<List<CategoryEntity>>
    {
        return runBlocking(Dispatchers.IO)
        {
            return@runBlocking dao.getAll()
        }
    }

    fun getByID(id:Int): CategoryEntity?
    {
        return runBlocking(Dispatchers.IO)
        {
            return@runBlocking dao.getByID(id)
        }
    }

    fun update(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.update(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insert(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.insert(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insertList(data: List<CategoryEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.insertAll(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun delete(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.delete(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun deleteAll():Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                dao.deleteAll()
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }
}