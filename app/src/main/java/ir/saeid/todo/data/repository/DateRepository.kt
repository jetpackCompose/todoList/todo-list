package ir.saeid.todo.data.repository

import ir.saeid.todo.App
import ir.saeid.todo.data.model.model.DateModel
import ir.saeid.todo.tool.CalendarTool
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateRepository @Inject constructor(private val calender: CalendarTool)
{


    fun getDayList(year: Int,month:Int):List<DateModel>
    {
        return runBlocking {
            val lstData : MutableList<DateModel> = arrayListOf()
            try
            {
                CoroutineScope(Dispatchers.IO).launch {
                    if(App.appLanguage == App.ENGLISH)
                    {
                        val lngMonth = arrayOf(1,3,5,7,8,10,12)
                        when(month)
                        {
                            in lngMonth ->
                            {
                                for (i in 1..31)
                                {
                                    calender.setGregorianDate(year,month,i)
                                    lstData.add(DateModel(i,calender.gregorianWeekDayStr,false))
                                }
                            }
                            2 ->
                            {
                                if (calender.IsLeap(year))
                                {
                                    for (i in 1..29)
                                    {
                                        calender.setGregorianDate(year,month,i)
                                        lstData.add(DateModel(i,calender.gregorianWeekDayStr,false))
                                    }
                                }
                                else
                                {
                                    for (i in 1..28)
                                    {
                                        calender.setGregorianDate(year,month,i)
                                        lstData.add(DateModel(i,calender.gregorianWeekDayStr,false))
                                    }
                                }
                            }
                            else ->
                            {
                                for (i in 1..30)
                                {
                                    calender.setGregorianDate(year,month,i)
                                    lstData.add(DateModel(i,calender.gregorianWeekDayStr,false))
                                }
                            }
                        }
                    }
                    else
                    {
                        if (month<=6)
                        {
                            for (i in 1..31)
                            {
                                calender.setIranianDate(year,month,i)
                                lstData.add(DateModel(i,calender.weekDayStr,false))
                            }
                        }
                        else if (month in 7..11)
                        {
                            for (i in 1..30)
                            {
                                calender.setIranianDate(year,month,i)
                                lstData.add(DateModel(i,calender.weekDayStr,false))
                            }
                        }
                        else
                        {
                            if (calender.IsLeap(year))
                            {
                                for (i in 1..30)
                                {
                                    calender.setIranianDate(year,month,i)
                                    lstData.add(DateModel(i,calender.weekDayStr,false))
                                }
                            }
                            else
                            {
                                for (i in 1..29)
                                {
                                    calender.setIranianDate(year,month,i)
                                    lstData.add(DateModel(i,calender.weekDayStr,false))
                                }
                            }
                        }
                    }
                }
            }
            catch (e:Exception)
            {
                e.printStackTrace()
            }
            return@runBlocking lstData
        }
    }

    fun getCurrentYear():Int
    {
        return runBlocking(Dispatchers.IO) {
            if(App.appLanguage == App.ENGLISH)
                return@runBlocking calender.gregorianYear
            else
                return@runBlocking calender.iranianYear
        }
    }

    fun getCurrentMonth():Int
    {
        return runBlocking(Dispatchers.IO) {
            if(App.appLanguage == App.ENGLISH)
                return@runBlocking calender.gregorianMonth
            else
                return@runBlocking calender.iranianMonth
        }
    }

    fun getCurrentDay():Int
    {
        return runBlocking(Dispatchers.IO) {
            if(App.appLanguage == App.ENGLISH)
                return@runBlocking calender.gregorianDay
            else
                return@runBlocking calender.iranianDay
        }
    }

    fun initializeCalendar(year:Int,month:Int,day:Int,isInitializedWithDevice : Boolean)
    {
        CoroutineScope(Dispatchers.IO).launch{
            if (isInitializedWithDevice)
            {
                calender.setGregorianDate(year,month,day)
            }
            else
            {
                if(App.appLanguage == App.ENGLISH)
                    calender.setGregorianDate(year,month,day)
                else
                    calender.setIranianDate(year,month,day)
            }

        }
    }

    fun getMonthsName():List<String>
    {
        return runBlocking(Dispatchers.IO) {
            if(App.appLanguage == App.ENGLISH)
                return@runBlocking calender.englishMonthStr.toList()
            else
                return@runBlocking calender.iraniMonthStr.toList()
        }
    }

    fun isLeap(year:Int):Boolean
    {
        return runBlocking(Dispatchers.IO) {
            return@runBlocking calender.IsLeap(year)
        }
    }

    fun getSplitDate():List<String>
    {
        return runBlocking(Dispatchers.IO)
        {
            return@runBlocking listOf(
                "${calender.gregorianYear}/${if (calender.gregorianMonth < 10)"0" else ""}${calender.gregorianMonth}/" +
                        "${if (calender.gregorianDay < 10)"0" else ""}${calender.gregorianDay}",
                "${calender.iranianYear}/${if (calender.iranianMonth < 10)"0" else ""}${calender.iranianMonth}/" +
                        "${if (calender.iranianDay < 10)"0" else ""}${calender.iranianDay}",
                calender.englishMonthStr[calender.gregorianMonth],
                calender.iraniMonthStr[calender.iranianMonth],
                "${if (calender.gregorianDay < 10)"0" else ""}${calender.gregorianDay}",
                "${if (calender.iranianDay < 10)"0" else ""}${calender.iranianDay}"
            )
        }
    }

    fun getCurrentDateTime():List<String>
    {
        return runBlocking(Dispatchers.IO)
        {
            val calendar = Calendar.getInstance().time
            val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault())
            val formattedDate = dateFormat.format(calendar).toString().replace(" " ,"")
            return@runBlocking listOf(
                //Item in list => Full Date,Time,Year,Month,Day
                formattedDate.substring(0,10),
                formattedDate.substring(10),
                formattedDate.substring(0,4),
                formattedDate.substring(5,7),
                formattedDate.substring(8,10)
            )
        }
    }

    fun getYearRange():Iterable<Int>
    {
        return runBlocking(Dispatchers.IO)
        {
            if(App.appLanguage == App.ENGLISH)
                return@runBlocking 2022..2122
            else
                return@runBlocking 1401..1500
        }
    }
}