package ir.saeid.todo.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.saeid.todo.App
import ir.saeid.todo.R
import ir.saeid.todo.data.database.TaskDB
import ir.saeid.todo.data.model.entity.CategoryEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Module
@InstallIn(SingletonComponent::class)
object DBModule
{
    private lateinit var taskDB: TaskDB

    private var callBack: RoomDatabase.Callback = object : RoomDatabase.Callback()
    {
        override fun onCreate(db: SupportSQLiteDatabase)
        {
            CoroutineScope(Dispatchers.IO).launch {
                val catIcon = arrayOf( R.drawable.ic_category_shoping,R.drawable.ic_category_sport,
                    R.drawable.ic_category_location,R.drawable.ic_category_drink,
                    R.drawable.ic_category_club,R.drawable.ic_category_other

                )
                val catTitle = arrayOf("Shopping","Sport","Location","Drink","Club","Other")
                val catColor = arrayOf("#FEA64C","#FE1E9A","#254DDE","#00FFFF","#FE1E9A","#181743")
                for (i in 0 until App.CATEGORY_COUNT)
                {
                    val value = CategoryEntity()
                    value.bgColor = catColor[i]
                    value.imageRes = catIcon[i]
                    value.title = catTitle[i]
                    value.selected = 0
                    taskDB.categoryDao().insert(value)
                }
            }
            // do something after database has been created
        }

        override fun onOpen(db: SupportSQLiteDatabase)
        {
            // do something every time database is open
        }
    }


    @Provides
    fun provideDB(@ApplicationContext appContext: Context): TaskDB
    {
         taskDB = Room.databaseBuilder(appContext, TaskDB::class.java,"TaskDB")
            .fallbackToDestructiveMigration()
            .addCallback(callBack)
            .build()
        return taskDB
    }
}

