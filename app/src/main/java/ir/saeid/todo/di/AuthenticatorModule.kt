package ir.saeid.todo.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.saeid.todo.data.auth.DataAuthenticator

@Module
@InstallIn(SingletonComponent::class)
object AuthenticatorModule
{
    @Provides
    fun provideAuth(@ApplicationContext appContext: Context): DataAuthenticator
    {
        return DataAuthenticator()
    }
}