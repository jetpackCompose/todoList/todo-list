package ir.saeid.todo.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.saeid.todo.tool.CalendarTool

@Module
@InstallIn(SingletonComponent::class)
object CalendarModule
{
    @Provides
    fun provideCalendar(@ApplicationContext appContext: Context): CalendarTool
    {
        return CalendarTool()
    }
}
