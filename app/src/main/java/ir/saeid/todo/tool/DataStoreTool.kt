package ir.saeid.todo.tool

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import ir.saeid.todo.App
import ir.saeid.todo.ui.theme.CustomTheme
import ir.saeid.todo.ui.theme.CustomThemeManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking




class DataStoreTool
{
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = App.PREFERENCE_NAME)

    fun saveLanguageValue(context : Context,value:String):Boolean
    {
        return runBlocking(Dispatchers.IO){
            try
            {
                context.dataStore.edit {
                    it[App.LANGUAGE_DATA_KEY] = value
                }
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun saveDarkModeValue(context : Context,value:Boolean)
    {
        CoroutineScope(Dispatchers.IO).launch {
            context.dataStore.edit {
                it[App.DARK_MODE_DATA_KEY] = value
            }
            if (value)
                CustomThemeManager.customTheme = CustomTheme.DARK
            else
                CustomThemeManager.customTheme = CustomTheme.LIGHT
        }
    }

    fun getLanguageValue(context : Context):String
    {
        return runBlocking(Dispatchers.IO)
        {
            context.dataStore.data.first()[App.LANGUAGE_DATA_KEY].let {
                if (it == null || it == "") return@runBlocking App.ENGLISH else it
            }
        }
    }

    fun getDarkModeValue(context : Context):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            context.dataStore.data.first()[App.DARK_MODE_DATA_KEY].let {
                it ?: return@runBlocking false
            }
        }
    }
}