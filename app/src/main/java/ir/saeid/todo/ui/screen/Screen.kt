package ir.saeid.todo.ui.screen

sealed class Screen(val route : String)
{
    object Splash:Screen(route = "Splash_Screen")
    object Home:Screen(route = "Main_Screen")
    object NewTask:Screen(route = "New_Task")
    object DoneList:Screen(route = "Done_Task")
}
