package ir.saeid.todo.ui.screen

import android.content.Context
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.chargemap.compose.numberpicker.ListItemPicker
import com.chargemap.compose.numberpicker.NumberPicker
import ir.saeid.todo.App
import ir.saeid.todo.R
import ir.saeid.todo.data.model.entity.TaskEntity
import ir.saeid.todo.helper.tryCatch
import ir.saeid.todo.ui.activity.MainActivity
import ir.saeid.todo.ui.component.*
import ir.saeid.todo.ui.screen.ErrorType.*
import ir.saeid.todo.ui.theme.CustomThemeManager
import ir.saeid.todo.viewmodel.MyViewModelProvider
import kotlinx.coroutines.*
import kotlin.math.abs

// Boolean State
private lateinit var showTimePicker :MutableState<Boolean>
private lateinit var showDatePicker :MutableState<Boolean>

// Int State
lateinit var monthRange : MutableState<Iterable<Int>>


// TextFieldValue State
private lateinit var titleValueState :MutableState<TextFieldValue>
private lateinit var descriptionValueState :MutableState<TextFieldValue>
private lateinit var timeValueState :MutableState<TextFieldValue>
private lateinit var dateValueState :MutableState<TextFieldValue>

// Main State
private lateinit var scope : CoroutineScope
private lateinit var scaffoldState: ScaffoldState

// Property List
val monthsName : MutableList<String> = mutableListOf()

// Other Property
private lateinit var focusManager : FocusManager
private var selectedCategory = -1
private lateinit var navController: NavController
private  var currentYear = 0
private  var currentMonth = 0
private  var currentDay = 0
private var firstRun = true
private var gotoBack = false
private var selectedYear = 0
private var selectedMonth = 0
private var selectedDay = 0

private lateinit var currentContext: Context
private lateinit var mainInstance : MainActivity
@Composable
fun NewTaskScreen(nav: NavController)
{
    //Boolean State
    showTimePicker = remember { mutableStateOf(false) }
    showDatePicker = remember { mutableStateOf(false) }

    //TextFieldValue State
    titleValueState = remember { mutableStateOf(TextFieldValue("")) }
    descriptionValueState = remember { mutableStateOf(TextFieldValue("")) }
    timeValueState = remember { mutableStateOf(TextFieldValue("")) }
    dateValueState = remember {  mutableStateOf(TextFieldValue(""))  }

    //List State


    //Main State
    scope = rememberCoroutineScope()
    scaffoldState = rememberScaffoldState()

    //Other
    focusManager = LocalFocusManager.current
    currentContext = LocalContext.current
    mainInstance = currentContext as MainActivity
    navController = nav

    tryCatch("NewTaskScreen","NewTaskScreen")
    {
        if (firstRun)
        {
            if (!gotoBack)
            {
                initializeCalendar()
                firstRun = false
            }
            else
            {
                gotoBack = false
            }
        }
    }

    Scaffold(
        modifier = Modifier
            .background(Color.Transparent),
        scaffoldState = scaffoldState,
        backgroundColor = CustomThemeManager.colors.bgScreen,
        topBar = {  },
        bottomBar = { BottomScreen() },
        content = {
            Surface(
                modifier = Modifier
                    .fillMaxSize(),
                color = CustomThemeManager.colors.bgScreen
            )
            {
                NewTaskBodyScreen()
            }
        },

        )
}

@Composable
fun NewTaskBodyScreen()
{
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 80.dp)
            .clip(RoundedCornerShape(topStart = 24.dp, bottomStart = 24.dp))
            .background(
                brush = CustomThemeManager.colors.bgNewTask,
                shape = RoundedCornerShape(topStart = 24.dp, bottomStart = 24.dp)
            ),
        contentAlignment = Alignment.Center
    )
    {

        Column(
            modifier = Modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        )
        {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.9f)
                    .verticalScroll(rememberScrollState())
                    .padding(horizontal = 16.dp, vertical = 8.dp)
            )
            {
                CreateNormalText(
                    textString = stringResource(id = R.string.newTask)
                    , align = TextAlign.Start,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.padding(top = 24.dp))
                CreateNormalText(
                    textString = stringResource(id = R.string.icon),
                    align = TextAlign.Start,
                )
                LazyRow(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp),
                    state = rememberLazyListState(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                )
                {
                    items(mainInstance.categoryItems,key = {it.id})
                    {
                        CreateCategoryButton(
                            buttonSize = 56,
                            item = it
                        )
                        {
                            for (i in mainInstance.categoryItems)
                            {
                                i.selected = 0
                            }
                            it.selected = 1
                            selectedCategory = it.id
                            mainInstance.categoryItems.swapList(mainInstance.categoryItems)
                        }
                    }
                }
                Spacer(modifier = Modifier.padding(top = 16.dp))

                CreateOutlinedTextField(
                    labelString = stringResource(id = R.string.title),
                    keyType = KeyboardType.Text,
                    length = 50,
                    isSingleLine = true,
                    maxLine = 1,
                    action = ImeAction.Next,
                    isReadOnly = false,
                    textState = titleValueState
                )

                Spacer(modifier = Modifier.padding(top = 16.dp))

                CreateOutlinedTextField(
                    labelString = stringResource(id = R.string.description),
                    keyType = KeyboardType.Text,
                    length = 500,
                    isSingleLine = false,
                    maxLine = 10,
                    action = ImeAction.Done,
                    isReadOnly = false,
                    textState = descriptionValueState
                )

                Spacer(modifier = Modifier.padding(top = 16.dp))

                CreateOutlinedTextField(
                    labelString = stringResource(id = R.string.date),
                    keyType = KeyboardType.Text,
                    length = 50,
                    isSingleLine = true,
                    maxLine = 1,
                    action = ImeAction.Next,
                    isReadOnly = true,
                    textState = dateValueState

                )
                {
                    showDatePicker.value = true
                }

                Spacer(modifier = Modifier.padding(top = 16.dp))

                CreateOutlinedTextField(
                    labelString = stringResource(id = R.string.time),
                    keyType = KeyboardType.Text,
                    length = 50,
                    isSingleLine = true,
                    maxLine = 1,
                    action = ImeAction.Next,
                    isReadOnly = true,
                    textState = timeValueState
                )
                {
                    showTimePicker.value = true
                }
            }



        }

        if (showTimePicker.value)
        {
            ShowTimePicker()
        }

        if (showDatePicker.value)
        {
            ShowDatePicker()
        }
    }
}


@Composable
fun BottomScreen()
{
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 80.dp, bottom = 16.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    )
    {
        CreatePositiveButton(
            text = stringResource(id = R.string.insert)
            , width = 120,
            height = 40
        )
        {
            scope.launch(Dispatchers.IO)
            {
                if (!MyViewModelProvider.authModel.authenticateInt(selectedCategory,-1))
                {
                    showError(CATEGORY_NOT_SELECTED)
                }
                else if (!MyViewModelProvider.authModel.authenticateText(titleValueState.value.text, App.MIN_TEXT_LENGTH))
                {
                    showError(TITLE_IS_SHORT)
                }
                else if (!MyViewModelProvider.authModel.authenticateText(descriptionValueState.value.text, App.MIN_TEXT_LENGTH))
                {
                    showError(DESCRIPTION_IS_SHORT)
                }
                else
                {
                    createNewTask()
                }
            }


        }
        CreateNegativeButton(
            text = stringResource(id = R.string.cancel),
            width = 120,
            height = 40
        )
        {
            firstRun = true
            gotoBack = true
            navController.popBackStack()
        }
    }
}


@Composable
fun ShowTimePicker()
{
    val time = timeValueState.value.text.replace(" ","").split(":")
    var hourValue by remember { mutableStateOf(time[0].toInt()) }
    var minuteValue by remember { mutableStateOf(time[1].toInt()) }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(start = 96.dp, end = 24.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(
                width = 2.dp,
                CustomThemeManager.colors.logoBorder,
                shape = RoundedCornerShape(16.dp)
            )
            .background(
                color = CustomThemeManager.colors.bgScreen,
                shape = RoundedCornerShape(16.dp)
            ),
    )
    {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
                ,
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            NumberPicker(
                value = hourValue,
                range = 0..23,
                onValueChange = {
                    hourValue = it
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
            NumberPicker(
                value = minuteValue,
                range = 0..59,
                onValueChange = {
                    minuteValue = it
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            CreatePositiveButton(
                text = stringResource(id = R.string.submit),
                width = 120,
                height = 40
            )
            {
                timeValueState.value = TextFieldValue("${if (abs(hourValue) < 10) "0" else ""}$hourValue:" +
                        "${if (abs(minuteValue) < 10) "0" else ""}$minuteValue")
                showTimePicker.value = false
                focusManager.clearFocus()
            }
        }

    }
}



@Composable
fun ShowDatePicker()
{

    val date = dateValueState.value.text.replace(" ","").split("/")
    var yearValueSate by remember { mutableStateOf(date[0].toInt()) }
    var monthValueSate by remember { mutableStateOf(date[1].toInt()) }
    var dayValueSate by remember { mutableStateOf(date[2].toInt()) }

    monthRange = remember {
        mutableStateOf(computeDayRange(yearValueSate,monthValueSate))
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(start = 16.dp, end = 24.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(
                width = 2.dp,
                CustomThemeManager.colors.logoBorder,
                shape = RoundedCornerShape(16.dp)
            )
            .background(
                color = CustomThemeManager.colors.bgScreen,
                shape = RoundedCornerShape(16.dp)
            ),
    )
    {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
            ,
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            NumberPicker(
                modifier = Modifier
                    .weight(0.33f),
                value = yearValueSate,
                range = currentYear..currentYear+5,
                onValueChange = {
                    yearValueSate = it
                    selectedYear = it
                    monthRange.value = computeDayRange(yearValueSate,monthValueSate)
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
            ListItemPicker(
                modifier = Modifier
                    .weight(0.33f),
                label  = { monthsName[it-1]},
                value = monthValueSate,
                onValueChange = {
                    monthValueSate = it
                    selectedMonth = it
                    monthRange.value = computeDayRange(yearValueSate,monthValueSate)
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                list = computeMonthRange(yearValueSate).toList(),
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
            NumberPicker(
                modifier = Modifier
                    .weight(0.33f),
                value = dayValueSate,
                range = monthRange.value,
                onValueChange = {
                    dayValueSate = it
                    selectedDay = it
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            CreatePositiveButton(
                text = stringResource(id = R.string.submit),
                width = 120,
                height = 40
            )
            {
                dateValueState.value = TextFieldValue("${yearValueSate}/" +
                        "${if (monthValueSate<10) "0" else ""}${monthValueSate}/" +
                        "${if (dayValueSate<10) "0" else ""}$dayValueSate")
                showDatePicker.value = false
                focusManager.clearFocus()
            }
        }
    }
}


fun initializeCalendar()
{

    scope.launch(Dispatchers.IO)
    {
        val currentDateTime = mutableListOf<String>()
        tryCatch("NewTaskScreen","initializeCalendar")
        {
            currentDateTime.addAll(MyViewModelProvider.dateModel.getCurrentDateTime())

            MyViewModelProvider.dateModel.initializeCalendar(currentDateTime[2].toInt(),currentDateTime[3].toInt(),currentDateTime[4].toInt(),true)
            monthsName.clear()
            monthsName.addAll(MyViewModelProvider.dateModel.getMonthsName())
            monthsName.removeAt(0)
            currentYear = MyViewModelProvider.dateModel.getCurrentYear()
            currentMonth = MyViewModelProvider.dateModel.getCurrentMonth()
            currentDay = MyViewModelProvider.dateModel.getCurrentDay()
            Log.e("3691", "Current Date Is : ${currentDateTime[2]}/${currentDateTime[3]}/${currentDateTime[4]} " )
            Log.e("3691", "Current Day Is : $currentDay " )
            selectedYear = currentYear
            selectedMonth= currentMonth
            selectedDay = currentDay
        }

        withContext(Dispatchers.Main){
            tryCatch("NewTaskScreen","initializeCalendar")
            {
                dateValueState.value = TextFieldValue("$currentYear/" +
                        "${if (currentMonth<10) "0" else ""}$currentMonth/" +
                        "${if (currentDay < 10) "0" else ""}$currentDay")
                timeValueState.value = TextFieldValue(currentDateTime[1])
            }
        }
    }
}


fun computeDayRange(year:Int,month:Int):Iterable<Int>
{
    val range : Iterable<Int>
    return runBlocking(Dispatchers.IO)
    {
        val isLeap = MyViewModelProvider.dateModel.isLeap(year)
        if (App.appLanguage == App.PERSIAN)
        {
            range = if (year == currentYear && month == currentMonth && month<=6) currentDay..31
            else if (year == currentYear && month == currentMonth && month == 12 && isLeap)  currentDay..30
            else if (year == currentYear && month == currentMonth && month == 12 && !isLeap)  currentDay..29
            else if (year == currentYear && month == currentMonth && month >=7 && month <12 )  currentDay..30
            else if (month<=6)  1..31
            else if (month == 12 && isLeap) 1..30
            else if (month == 12 && !isLeap)  1..29
            else if (month in 7..11)  1..30
            else   1..30
        }
        else if (App.appLanguage == App.ENGLISH)
        {
            val lngMonth = arrayOf(1,3,5,7,8,10,12)
            range = if (year == currentYear && month == currentMonth && month in lngMonth) currentDay..31
            else if (year == currentYear && month == currentMonth && month == 2 && isLeap)  currentDay..29
            else if (year == currentYear && month == currentMonth && month == 2 && !isLeap)  currentDay..28
            else if (year == currentYear && month == currentMonth && month !in lngMonth)  currentDay..30
            else if (month in lngMonth)  1..31
            else if (month == 2 && isLeap) 1..29
            else if (month == 2 && !isLeap)  1..28
            else if ( month !in lngMonth)  1..30
            else   1..30
        }
        else
        {
            range = 1..30
        }
        Log.e("3691", range.toString() )
        return@runBlocking range
    }
}

fun computeMonthRange(year:Int):Iterable<Int>
{
    val range : Iterable<Int>
    return runBlocking(Dispatchers.IO)
    {
        range = if (year == currentYear) currentMonth..12
        else   1..12
        return@runBlocking range
    }
}


fun showError(type : ErrorType)
{
    val errorTitle = currentContext.getString(R.string.notice)
    when(type)
    {
        CATEGORY_NOT_SELECTED ->
        {
            val errorMessage = currentContext.getString(R.string.CATEGORY_NOT_SELECTED)
            scope.launch(Dispatchers.Main)
            {
                scaffoldState.snackbarHostState.showSnackbar(message = errorMessage,actionLabel = errorTitle, duration = SnackbarDuration.Short)
            }
        }
        TITLE_IS_SHORT ->
        {
            val errorMessage = currentContext.getString(R.string.TITLE_IS_SHORT)
            scope.launch(Dispatchers.Main)
            {
                scaffoldState.snackbarHostState.showSnackbar(message = errorMessage,actionLabel = errorTitle, duration = SnackbarDuration.Short)
            }
        }
        DESCRIPTION_IS_SHORT ->
        {
            val errorMessage = currentContext.getString(R.string.DESCRIPTION_IS_SHORT)
            scope.launch(Dispatchers.Main)
            {
                scaffoldState.snackbarHostState.showSnackbar(message = errorMessage,actionLabel = errorTitle, duration = SnackbarDuration.Short)
            }
        }
        NOTHING -> {}
    }
}

fun createNewTask()
{
    scope.launch(Dispatchers.IO)
    {
        val entity = TaskEntity()
        MyViewModelProvider.dateModel.initializeCalendar(selectedYear, selectedMonth, selectedDay,false)
        val date = MyViewModelProvider.dateModel.getSplitDate()
        entity.categoryID = selectedCategory
        entity.title = MyViewModelProvider.authModel.convertToEng(titleValueState.value.text)
        entity.description = MyViewModelProvider.authModel.convertToEng(descriptionValueState.value.text)
        entity.enDate = MyViewModelProvider.authModel.convertToEng(date[0])
        entity.irDate = MyViewModelProvider.authModel.convertToEng(date[1])
        entity.enMonthName = date[2]
        entity.irMonthName = date[3]
        entity.enDay = MyViewModelProvider.authModel.convertToEng(date[4])
        entity.irDay = MyViewModelProvider.authModel.convertToEng(date[5])
        entity.time = MyViewModelProvider.authModel.convertToEng(timeValueState.value.text)
        entity.doneState = 0
        entity.selected = 0
        if (MyViewModelProvider.taskModel.insert(entity))
        {
            scaffoldState.snackbarHostState.showSnackbar(currentContext.getString(R.string.insertSuccessFull), duration = SnackbarDuration.Short)
            runBlocking(Dispatchers.Main){
                firstRun = true
                gotoBack = true
                navController.popBackStack()
            }

        }
    }
}


enum class ErrorType
{
    CATEGORY_NOT_SELECTED,
    TITLE_IS_SHORT,
    DESCRIPTION_IS_SHORT,
    NOTHING
}


