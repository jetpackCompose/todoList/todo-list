package ir.saeid.todo.ui.theme

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

private val customLightColors = CustomColor(
    black = Color(0xFF000000),
    white = Color(0xFFFFFFFF),
    bgScreen = Color(0xFFFFFFFF),
    logoBorder = Color(0xFF95989A),
    bgLogo = Brush.linearGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFFFFFFFF))),
    textColor = Color(0xFF181743),
    bgMain = Brush.verticalGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFF6060CC))),
    bgCalenderItemTopSelected = Brush.linearGradient(colors = listOf(Color(0xFF00FFFF),Color(0xFF254DDE))),
    bgCalenderItemTopUnSelected = Brush.linearGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFFFFFFFF))),
    calenderItemSelectedTextColor = Color(0xFFFFFFFF),
    calenderItemUnSelectedTextColor = Color(0xFF181743),
    bgNewTask = Brush.linearGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFFCAEBFE))),
    focusedBorderColor = Color(0xFF80DEEA),
    positiveButton = Brush.linearGradient(colors = listOf(Color(0xFF00FFFF),Color(0xFF254DDE))),
    negativeButton = Brush.linearGradient(colors = listOf(Color(0xFFFF5E5E),Color(0xFFDE2525))),
    buttonTextColor = Color(0xFFFFFFFF),
    dividerColor = Color(0xFFFEA64C),
    swipeColor = Color(0xFFF44336),
    drawerColor = Brush.verticalGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFF6200EA))),
    bgButtonColor =  Color(0xFF6200EA),
    bgTaskColor =  Brush.linearGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFFFFFFFF))),
)

private val customDarkColors = CustomColor(
    black = Color(0xFF000000),
    white = Color(0xFFFFFFFF),
    bgScreen = Color(0xFF1B1F23),
    logoBorder = Color(0xFF6200EA),
    bgLogo = Brush.linearGradient(colors = listOf(Color(0xFFFEA64C),Color(0xFFFE1E9A),Color(0xFF254DDE),Color(0xFF00FFFF))),
    textColor = Color(0xFFFFFFFF),


    bgMain = Brush.verticalGradient(colors = listOf(Color(0xFF272C31),Color(0xFF000000))),
    bgCalenderItemTopSelected = Brush.linearGradient(colors = listOf(Color(0xFF00FFFF),Color(0xFF254DDE))),
    bgCalenderItemTopUnSelected = Brush.linearGradient(colors = listOf(Color(0xFFFFFFFF),Color(0xFFFFFFFF))),
    calenderItemSelectedTextColor = Color(0xFFFFFFFF),
    calenderItemUnSelectedTextColor = Color(0xFF181743),
    bgNewTask = Brush.linearGradient(colors = listOf(Color(0xFF272C31),Color(0xFF000000))),
    focusedBorderColor = Color(0xFF6200EA),
    positiveButton = Brush.linearGradient(colors = listOf(Color(0xFF00FFFF),Color(0xFF254DDE))),
    negativeButton = Brush.linearGradient(colors = listOf(Color(0xFFFF5E5E),Color(0xFFDE2525))),
    buttonTextColor = Color(0xFFFFFFFF),
    dividerColor = Color(0xFFFF4A4A),
    swipeColor = Color(0xFFFF3232),

    drawerColor = Brush.verticalGradient(colors = listOf(Color(0xFF272C31),Color(0xFF000000))),
    bgButtonColor =  Color(0xFFFF1744),
    bgTaskColor =  Brush.linearGradient(colors = listOf(Color(0xFF6200EA),Color(0xFF304FFE))),
)

private val localColorsProvider = staticCompositionLocalOf { customLightColors }

@Composable
fun customLocalProvider(
    colors:CustomColor,
    content: @Composable () -> Unit
)
{
    val colorPalette = remember { colors.initColor()}
    colorPalette.update(colors)
    CompositionLocalProvider( localColorsProvider provides  colorPalette, content = content)
}

object CustomThemeManager{
    val colors:CustomColor
        @Composable
        get() = localColorsProvider.current

    var customTheme by mutableStateOf(CustomTheme.LIGHT)

    fun isSystemIsDarkTheme() : Boolean
    {
        return customTheme == CustomTheme.DARK
    }
}

private val CustomTheme.colors:Pair<Colors,CustomColor>
    get() = when(this)
    {
        CustomTheme.DARK -> darkColors() to customDarkColors
        CustomTheme.LIGHT -> lightColors() to customLightColors
    }

@Composable
fun TODOTheme(customTheme: CustomTheme = CustomThemeManager.customTheme, content: @Composable () -> Unit)
{
    val (colorPalette,myColor) = customTheme.colors

    customLocalProvider(colors = myColor) {
        MaterialTheme(
            colors = colorPalette,
            typography = Typography,
            shapes = Shapes,
            content = content
        )
    }
}