package ir.saeid.todo.ui.screen

import android.os.Handler
import android.os.Looper
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ir.saeid.todo.App
import ir.saeid.todo.BuildConfig
import ir.saeid.todo.R
import ir.saeid.todo.ui.component.CreateNormalText
import ir.saeid.todo.ui.theme.CustomThemeManager

private lateinit var runnable : Runnable
private var firstTime = true
@Composable
fun SplashScreen(navController: NavController)
{
    val scaffoldState = rememberScaffoldState()
    val handler = Handler(Looper.getMainLooper())
    runnable = Runnable {

        if (firstTime)
        {
            firstTime = false
            navController.popBackStack()
            navController.navigate(route = Screen.Home.route)
            handler.removeCallbacks(runnable)
        }

    }
    Scaffold(
        scaffoldState = scaffoldState,
        backgroundColor = CustomThemeManager.colors.bgScreen,
        topBar = { },
        bottomBar = { BottomContent() },
        content = { padding ->
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(padding),
                color = CustomThemeManager.colors.bgScreen
            )
            {
                BodyContent()
            }
        }
    )
    handler.postDelayed(runnable, App.SPLASH_SCREEN_TIME)
}

@Composable
fun BodyContent()
{
    val logoRes = if (CustomThemeManager.isSystemIsDarkTheme()) R.drawable.ic_logo_white else R.drawable.ic_logo_colored
    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
        )
    {
        Box(

            modifier = Modifier
                .size(128.dp)
                .clip(RoundedCornerShape(8.dp))
                .border(
                    BorderStroke(width = 1.dp, color = CustomThemeManager.colors.logoBorder),
                    shape = RoundedCornerShape(8.dp)
                )
                .background(CustomThemeManager.colors.bgLogo)

        )
        {
            Image(
                painter = painterResource(id = logoRes),
                contentDescription = "",
                modifier = Modifier
                    .padding(16.dp)
                    .background(Color.Transparent)
            )
        }
    }
}

@Composable
fun BottomContent()
{
    Row(
        modifier = Modifier.padding(16.dp)
    )
    {
        CreateNormalText(
            textString = stringResource(id = R.string.AppVersion,BuildConfig.VERSION_NAME),
            align = TextAlign.End
            )
    }

}
