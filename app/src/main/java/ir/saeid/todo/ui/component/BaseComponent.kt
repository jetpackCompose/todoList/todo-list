package ir.saeid.todo.ui.component

import androidx.annotation.DrawableRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ir.saeid.todo.App
import ir.saeid.todo.R
import ir.saeid.todo.data.model.entity.CategoryEntity
import ir.saeid.todo.data.model.entity.TaskEntity
import ir.saeid.todo.data.model.model.DateModel
import ir.saeid.todo.ui.screen.TaskType
import ir.saeid.todo.ui.theme.CustomThemeManager


@Composable
fun CreateNormalText(
    textString: String,
    align: TextAlign,
    textColor:Color = CustomThemeManager.colors.textColor,
    fontWeight: FontWeight = FontWeight.Normal
)
{
    Text(
        text = textString,
        style = LocalTextStyle.current.copy(textAlign = align),
        textAlign = align,
        color = textColor,
        fontSize = 16.sp,
        fontWeight = fontWeight,
        fontStyle = FontStyle.Normal,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}

@Composable
fun CreateCircleButton(buttonSize: Int, imageRes: Int, onClickListener: () -> Unit)
{
    IconButton(
        modifier = Modifier
            .size(buttonSize.dp)
            .clip(CircleShape)
            .padding(0.dp),
        onClick = onClickListener
    )
    {
        Image( painter = painterResource(id = imageRes),
            contentDescription = "",
            modifier = Modifier
                .size(buttonSize.dp)
                .clip(CircleShape)
                .padding(0.dp),
            contentScale = ContentScale.Inside
        )
    }
}


@Composable
fun CreateCategoryButton(buttonSize: Int, item : CategoryEntity, onClickListener: () -> Unit)
{
    Spacer(modifier = Modifier.padding(start = 8.dp))
    IconButton(
        modifier = Modifier
            .size(buttonSize.dp)
            .clip(CircleShape)
            .padding(0.dp),
        onClick = onClickListener
    )
    {
        Image( painter = painterResource(id = item.imageRes),
            contentDescription = "",
            modifier = Modifier
                .size(buttonSize.dp)
                .clip(CircleShape)
                .border(
                    width = 2.dp,
                    color = if (item.selected == 1) Color.Green else Color.Transparent,
                    shape = CircleShape
                )
                .padding(0.dp),
            contentScale = ContentScale.Fit
        )
    }
}


@Composable
fun CreateToDoItem(task : TaskEntity, type : TaskType,@DrawableRes imageRes: Int,bgColor : String, checkedChange : (() -> Unit) = {})
{
    val color = android.graphics.Color.parseColor(bgColor)
    when(type)
    {
        TaskType.NORMAL ->
        {
            CreateNormalModeToDoItem(task,imageRes,Color(color))
        }
        TaskType.CALENDAR ->
        {
            CreateCalendarModeToDoItem(task, imageRes,Color(color))
        }
        TaskType.DONE ->
        {
            CreateDoneModeToDoItem(task,imageRes,Color(color),checkedChange)
        }
    }

}

@Composable
fun CreateNormalModeToDoItem(todo: TaskEntity, @DrawableRes imageRes: Int, bgColor : Color)
{
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(96.dp)
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(CustomThemeManager.colors.bgTaskColor),
    )
    {
        Row(
            modifier = Modifier
                .width(16.dp)
                .height(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        )
        {
            Text(
                modifier = Modifier
                    .size(8.dp)
                    .clip(CircleShape)
                    .background(bgColor),
                text = ""
            )
        }

        Row(
            modifier = Modifier
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        )
        {
            Column(
                modifier = Modifier
                    .weight(0.15f),
                verticalArrangement = Arrangement.Center
            )
            {
                Image(
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .size(72.dp)
                    ,
                    painter = painterResource(id = imageRes),
                    contentDescription =""
                )
            }
            Column(
                modifier = Modifier
                    .weight(0.65f)
                    .padding(start = 8.dp, end = 8.dp),
                verticalArrangement = Arrangement.Center
            )
            {
                CreateNormalText(textString = todo.title, align = TextAlign.Start)
            }
            Column(
                modifier = Modifier
                    .weight(0.2f)
                    .padding(end = 8.dp)
                    .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            )
            {
                CreateNormalText(
                    textString = if (App.appLanguage == App.ENGLISH)todo.enDay + " " + todo.enMonthName else todo.irDay + " " + (if(todo.irMonthName.length>3) todo.irMonthName.subSequence(0,3) else todo.irMonthName),
                    align = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )
                CreateNormalText(
                    textString = todo.time,
                    align = TextAlign.Center,
                )
            }
        }
    }
}

@Composable
fun CreateCalendarModeToDoItem(todo: TaskEntity,imageRes: Int,bgColor : Color)
{
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(96.dp)
            .padding(horizontal = 16.dp, vertical = 8.dp),
        //.background(Color.Red)
    )
    {

        Row(
            modifier = Modifier
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        )
        {
            CreateNormalText(
                textString = todo.time,
                align = TextAlign.Start,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.padding(start = 8.dp))
            Text(
                modifier = Modifier
                    .size(8.dp)
                    .clip(CircleShape)
                    .background(bgColor),
                text = ""
            )
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(start = 8.dp)
                    .clip(RoundedCornerShape(8.dp))
                    .background(CustomThemeManager.colors.bgScreen),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly
            )
            {
                Column(
                    modifier = Modifier
                        .weight(0.15f),
                    verticalArrangement = Arrangement.Center
                )
                {
                    Image(
                        modifier = Modifier
                            .size(72.dp)
                            .padding(start = 8.dp)
                            .clip(RoundedCornerShape(8.dp)),
                        painter = painterResource(id = imageRes),
                        contentDescription =""
                    )
                }
                Column(
                    modifier = Modifier
                        .padding(start = 8.dp, end = 8.dp)
                        .weight(0.85f),
                    verticalArrangement = Arrangement.Center
                )
                {
                    CreateNormalText(textString = todo.title, align = TextAlign.Start)
                }
            }

        }
    }
}

@Composable
fun CreateDoneModeToDoItem(todo: TaskEntity,imageRes: Int,bgColor : Color, checkedChange : (() -> Unit))
{
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(96.dp)
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(CustomThemeManager.colors.bgScreen),
    )
    {
        Row(
            modifier = Modifier
                .width(16.dp)
                .height(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        )
        {
            Text(
                modifier = Modifier
                    .size(8.dp)
                    .clip(CircleShape)
                    .background(bgColor),
                text = ""
            )
        }

        Row(
            modifier = Modifier
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        )
        {
            Column(
                modifier = Modifier
                    .weight(0.15f),
                verticalArrangement = Arrangement.Center
            )
            {
                Image(
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .size(72.dp)
                        .clip(RoundedCornerShape(8.dp))
                    ,
                    painter = painterResource(id = imageRes),
                    contentDescription =""
                )
            }
            Column(
                modifier = Modifier
                    .padding(start = 8.dp, end = 8.dp)
                    .weight(0.70f),
                verticalArrangement = Arrangement.Center
            )
            {
                CreateNormalText(textString = todo.title, align = TextAlign.Start)
            }
            Column(
                modifier = Modifier
                    .weight(0.15f)
                    .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            )
            {
                Box(
                    modifier = Modifier
                        .size(32.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .border(
                            BorderStroke(1.dp, CustomThemeManager.colors.logoBorder),
                            RoundedCornerShape(8.dp)
                        )
                        .padding(4.dp)
                        .clickable { checkedChange() },
                /*    onClick = checkedChange,
                    shape = RoundedCornerShape(8.dp),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent, contentColor = Color.White),
                    border = */
                )
                {
                    if (todo.selected == 1)
                    {
                        Image(
                            modifier = Modifier
                                .fillMaxSize(),
                            painter = painterResource(id = R.drawable.ic_checkbox_checked),
                            contentDescription = ""
                        )

                    }
                }
             /*   Checkbox(
                    checked = todo.selected == 1,
                    onCheckedChange = checkedChange,
                    colors = CheckboxDefaults.colors(
                        checkedColor =
                    )
                )*/
            }
        }
    }
}


@Composable
fun CreateCalendarItem( dateModel:DateModel,onClickListener: () -> Unit)
{
    Row(
        modifier = Modifier
            .padding(4.dp)
    )
    {
        Button(
            modifier = Modifier
                .size(64.dp)
                .clip(RoundedCornerShape(8.dp))
                .background(
                    if (dateModel.isSelected) CustomThemeManager.colors.bgCalenderItemTopSelected else CustomThemeManager.colors.bgCalenderItemTopUnSelected
                )
                ,
            elevation = ButtonDefaults.elevation(0.dp),
            shape = RoundedCornerShape(8.dp),
            border = null,
            onClick = onClickListener,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Transparent,

            )
        )
        {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            )
            {
                CreateNormalText(textString = dateModel.day.toString(), align = TextAlign.Start,
                    textColor = if (dateModel.isSelected) CustomThemeManager.colors.calenderItemSelectedTextColor
                    else CustomThemeManager.colors.calenderItemUnSelectedTextColor,
                    fontWeight = FontWeight.Bold
                )
                CreateNormalText(
                    textString = dateModel.dayName,
                    align = TextAlign.Start,
                    textColor = if (dateModel.isSelected) CustomThemeManager.colors.calenderItemSelectedTextColor
                    else CustomThemeManager.colors.calenderItemUnSelectedTextColor,
                )
            }
        }
    }

}

@Composable
fun CreateOutlinedTextField(labelString: String, keyType: KeyboardType, length:Int, isSingleLine:Boolean, maxLine:Int, action: ImeAction, isReadOnly:Boolean,textState : MutableState<TextFieldValue>? = null, onClickListener: () -> Unit = {})
{
    val textValue : MutableState<TextFieldValue> = textState ?: remember { mutableStateOf(TextFieldValue("")) }
    val focusManager = LocalFocusManager.current
    OutlinedTextField(
        modifier = Modifier
            .onFocusChanged {
                            if (it.isFocused) onClickListener()
            },
        value = textValue.value ,
        onValueChange =  {
            if (it.text.length <= length) textValue.value = it
        },
        label = { CreateNormalText(labelString, TextAlign.Start) },
        keyboardOptions = KeyboardOptions(keyboardType = keyType, imeAction = action),
        textStyle = LocalTextStyle.current.copy(
            textAlign = TextAlign.Start,
            fontStyle = FontStyle.Normal,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
        ),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor =CustomThemeManager.colors.focusedBorderColor,
            textColor = CustomThemeManager.colors.textColor
        ),
        maxLines = maxLine,
        singleLine = isSingleLine,
        readOnly = isReadOnly,
        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus()})
    )
}

@Composable
fun CreatePositiveButton(text:String,width:Int,height:Int,onClickListener: () -> Unit)
{
    Button(
        modifier = Modifier
            .width(width.dp)
            .height(height.dp)
            .clip(RoundedCornerShape(24.dp))
            .background(
                brush = CustomThemeManager.colors.positiveButton,
                shape = RoundedCornerShape(24.dp)
            )
        ,
        shape = RoundedCornerShape(24.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent,
            contentColor = CustomThemeManager.colors.buttonTextColor
        ),
        elevation = ButtonDefaults.elevation(0.dp),
        onClick = onClickListener

    )
    {
        CreateNormalText(
            textString = text,
            align = TextAlign.Center,
            textColor = CustomThemeManager.colors.buttonTextColor
        )
    }
}

@Composable
fun CreateNegativeButton(text:String,width:Int,height:Int,onClickListener: () -> Unit)
{
    Button(
        modifier = Modifier
            .width(width.dp)
            .height(height.dp)
            .clip(RoundedCornerShape(24.dp))
            .background(
                brush = CustomThemeManager.colors.negativeButton,
                shape = RoundedCornerShape(24.dp)
            )
        ,
        shape = RoundedCornerShape(24.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent,
            contentColor = CustomThemeManager.colors.buttonTextColor
        ),
        elevation = ButtonDefaults.elevation(0.dp),
        onClick = onClickListener

    )
    {
        CreateNormalText(
            textString = text,
            align = TextAlign.Center,
            textColor = CustomThemeManager.colors.buttonTextColor
        )
    }
}


@Composable
fun CreateSpinner(modifier: Modifier, list : List<String>,onItemClickListener: (label : String) -> Unit)
{
    var expanded by remember { mutableStateOf(false) }
    var textValueState by remember { mutableStateOf(
        if (App.appLanguage == App.ENGLISH) list[1] else list[0] )
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.wrapContentSize()
    )
    {
        Button(
            modifier = modifier,
            onClick = { expanded = !expanded },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = CustomThemeManager.colors.bgButtonColor
            )
        )
        {
            CreateNormalText(
                textString = textValueState,
                align = TextAlign.Start,
                textColor = CustomThemeManager.colors.white
            )
            Icon(
                imageVector = Icons.Filled.ArrowDropDown,
                contentDescription = "",
                tint = CustomThemeManager.colors.white
            )
        }
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
        )
        {
            list.forEach { label ->
                DropdownMenuItem(onClick = {
                    onItemClickListener(label)
                    expanded = false
                    textValueState =   if (App.appLanguage == App.ENGLISH) list[1] else list[0]
                }
                )
                {
                    Text(text = label)
                }
            }
        }
    }
}