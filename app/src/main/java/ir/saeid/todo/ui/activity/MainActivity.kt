package ir.saeid.todo.ui.activity

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.saeid.todo.App
import ir.saeid.todo.R
import ir.saeid.todo.data.model.entity.CategoryEntity
import ir.saeid.todo.data.model.entity.TaskEntity
import ir.saeid.todo.data.model.model.DateModel
import ir.saeid.todo.helper.tryCatch
import ir.saeid.todo.tool.MyContextWrapper
import ir.saeid.todo.ui.screen.*
import ir.saeid.todo.ui.theme.TODOTheme
import ir.saeid.todo.viewmodel.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlin.system.exitProcess


@AndroidEntryPoint
class MainActivity : ComponentActivity()
{
    private lateinit var navController : NavHostController
    private var doubleBackToExit = false
    lateinit var taskStateList : SnapshotStateList<TaskEntity>
    lateinit var days : SnapshotStateList<DateModel>
    lateinit var  categoryItems : SnapshotStateList<CategoryEntity>
    lateinit var doneTaskStateList : SnapshotStateList<TaskEntity>
    override fun onCreate(savedInstanceState: Bundle?) 
    {
        super.onCreate(savedInstanceState)
        MyViewModelProvider.initial(this)

        setContent{
            TODOTheme{
                taskStateList = remember { mutableStateListOf() }
                days = remember { mutableStateListOf() }
                categoryItems = remember { mutableStateListOf() }
                doneTaskStateList = remember { mutableStateListOf() }
                initialObserve()
                navController = rememberNavController()
                navController.enableOnBackPressed(true)
                BackHandler(enabled = true) {
                    handleBackPressed()
                }
                SetupNav(navController = navController)
            }
        }

    }
    fun restartApp()
    {
        val packageManager: PackageManager = packageManager
        val intent = packageManager.getLaunchIntentForPackage(packageName)
        val componentName = intent!!.component
        val mainIntent = Intent.makeRestartActivityTask(componentName)
        startActivity(mainIntent)
        Runtime.getRuntime().exit(0)
    }

    override fun attachBaseContext(newBase: Context?)
    {
        super.attachBaseContext(newBase?.let { MyContextWrapper.wrap(it,App.appLanguage) })
    }

    private fun handleBackPressed()
    {
        try
        {
            val currentRoute = navController.currentBackStackEntry?.destination?.route
            if (currentRoute != null && currentRoute == Screen.Home.route)
            {
                if (doubleBackToExit)
                {
                    finishAffinity()
                    exitProcess(0)
                }
                doubleBackToExit = true
                Toast.makeText(this, getString(R.string.doubleBackToExit), Toast.LENGTH_SHORT).show()
                Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExit = false }, 2000)
            }
            else
                navController.popBackStack()

        }
        catch (e:Exception)
        {
            e.printStackTrace()
        }
    }

    private fun initialObserve()
    {
        tryCatch("MainActivity","initialObserve")
        {
            lifecycle.coroutineScope.launch{
                MyViewModelProvider.taskModel.tasks
                    .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                    .onEach { data ->
                        if(::taskStateList.isInitialized)taskStateList.clear()
                        if (data.isNotEmpty())
                        {
                            taskStateList.addAll(data)
                            taskStateList.swapList(data)
                        }
                    }
                    .launchIn(lifecycleScope)

                MyViewModelProvider.taskModel.getAllTaskIsDone().collect { data ->
                    if(::doneTaskStateList.isInitialized)doneTaskStateList.clear()
                    if (data.isNotEmpty())
                    {
                        doneTaskStateList.addAll(data)
                        doneTaskStateList.swapList(data)
                    }
                }

                }
            }

            MyViewModelProvider.dateModel.days.observe(this) { data ->
                tryCatch("MainActivity","initialObserve")
                {
                    days.clear()
                    days.addAll(data)
                    for (item in days)
                    {
                        if (item.day == selectedDayInMainScreen) item.isSelected = true
                    }
                    days.swapList(data)
                }
            }

            MyViewModelProvider.categoryModel.getAllData().observe(this) { data ->
                tryCatch("MainActivity","initialObserve")
                {
                    categoryItems.clear()
                    categoryItems.addAll(data)
                }
            }
        }
}




