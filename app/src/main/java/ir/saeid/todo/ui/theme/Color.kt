package ir.saeid.todo.ui.theme

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

enum class CustomTheme{
    DARK,LIGHT
}

@Stable
class CustomColor(
    black : Color,
    white : Color,
    bgScreen : Color,
    logoBorder : Color,
    bgLogo : Brush,
    textColor : Color,
    bgMain : Brush,
    bgCalenderItemTopSelected : Brush,
    bgCalenderItemTopUnSelected : Brush,
    calenderItemSelectedTextColor : Color,
    calenderItemUnSelectedTextColor : Color,
    bgNewTask : Brush,
    focusedBorderColor : Color,
    positiveButton : Brush,
    negativeButton : Brush,
    buttonTextColor : Color,
    dividerColor : Color,
    swipeColor : Color,
    drawerColor : Brush,
    bgButtonColor : Color,
    bgTaskColor : Brush,
)
{
    var black by mutableStateOf(black)
        private set
    var white by mutableStateOf(white)
        private set
    var bgScreen by mutableStateOf(bgScreen)
        private set
    var logoBorder by mutableStateOf(logoBorder)
        private set
    var bgLogo by mutableStateOf(bgLogo)
        private set
    var textColor by mutableStateOf(textColor)
        private set
    var bgMain by mutableStateOf(bgMain)
        private set
    var bgCalenderItemTopSelected by mutableStateOf(bgCalenderItemTopSelected)
        private set
    var bgCalenderItemTopUnSelected by mutableStateOf(bgCalenderItemTopUnSelected)
        private set
    var calenderItemSelectedTextColor by mutableStateOf(calenderItemSelectedTextColor)
        private set
    var calenderItemUnSelectedTextColor by mutableStateOf(calenderItemUnSelectedTextColor)
        private set
    var bgNewTask by mutableStateOf(bgNewTask)
        private set
    var focusedBorderColor by mutableStateOf(focusedBorderColor)
        private set
    var positiveButton by mutableStateOf(positiveButton)
        private set
    var negativeButton by mutableStateOf(negativeButton)
        private set
    var buttonTextColor by mutableStateOf(buttonTextColor)
        private set
    var dividerColor by mutableStateOf(dividerColor)
        private set
    var swipeColor by mutableStateOf(swipeColor)
        private set
    var drawerColor by mutableStateOf(drawerColor)
        private set
    var bgButtonColor by mutableStateOf(bgButtonColor)
        private set
    var bgTaskColor by mutableStateOf(bgTaskColor)
        private set

    fun update(colors:CustomColor)
    {
        this.black = colors.black
        this.white = colors.white
        this.bgScreen = colors.bgScreen
        this.logoBorder = colors.logoBorder
        this.bgLogo = colors.bgLogo
        this.textColor = colors.textColor
        this.bgMain = colors.bgMain
        this.bgCalenderItemTopSelected = colors.bgCalenderItemTopSelected
        this.bgCalenderItemTopUnSelected = colors.bgCalenderItemTopUnSelected
        this.calenderItemSelectedTextColor = colors.calenderItemSelectedTextColor
        this.calenderItemUnSelectedTextColor = colors.calenderItemUnSelectedTextColor
        this.bgNewTask = colors.bgNewTask
        this.focusedBorderColor = colors.focusedBorderColor
        this.positiveButton = colors.positiveButton
        this.negativeButton = colors.negativeButton
        this.buttonTextColor = colors.buttonTextColor
        this.dividerColor = colors.dividerColor
        this.swipeColor = colors.swipeColor
        this.drawerColor = colors.drawerColor
        this.bgButtonColor = colors.bgButtonColor
        this.bgTaskColor = colors.bgTaskColor
    }

    fun initColor() = CustomColor(black,white,bgScreen,logoBorder,bgLogo,textColor,bgMain,
        bgCalenderItemTopSelected,bgCalenderItemTopUnSelected,calenderItemSelectedTextColor,
        calenderItemUnSelectedTextColor,bgNewTask,focusedBorderColor,positiveButton,negativeButton,
        buttonTextColor,dividerColor,swipeColor,drawerColor,bgButtonColor,bgTaskColor
    )
}




val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)