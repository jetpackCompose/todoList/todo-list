package ir.saeid.todo.ui.screen

import android.content.Context
import android.util.Log
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.chargemap.compose.numberpicker.ListItemPicker
import com.chargemap.compose.numberpicker.NumberPicker
import ir.saeid.todo.App
import ir.saeid.todo.R
import ir.saeid.todo.ui.activity.MainActivity
import ir.saeid.todo.ui.component.*
import ir.saeid.todo.ui.theme.CustomThemeManager
import ir.saeid.todo.viewmodel.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// Boolean State
private lateinit var calenderMode : MutableState<Boolean>
private lateinit var showDatePicker : MutableState<Boolean>
private lateinit var showChangeLanguageAlert : MutableState<Boolean>
private lateinit var doneMode : MutableState<Boolean>

// String State
private lateinit var titleText : MutableState<String>
private lateinit var selectedDateState : MutableState<String>

// Property List
val lstMonthsName : MutableList<String> = mutableListOf()


// Main State
private lateinit var scope : CoroutineScope
private lateinit var scaffoldState: ScaffoldState

// Other Property
private lateinit var navController :NavController
 var selectedDayInMainScreen = 1
private var selectedYear = 0
private var selectedMonth = 0
private lateinit var currentContext: Context
private var firstRun = true
private lateinit var mainInstance : MainActivity
@Composable
fun MainScreen(nav: NavController)
{
    currentContext = LocalContext.current
    mainInstance = (currentContext as MainActivity)
    scaffoldState = rememberScaffoldState()
    calenderMode = remember { mutableStateOf(false) }
    showDatePicker = remember { mutableStateOf(false) }
    showChangeLanguageAlert = remember { mutableStateOf(false) }
    doneMode = remember { mutableStateOf(false) }
    titleText = remember { mutableStateOf( "TODO") }
    selectedDateState = remember { mutableStateOf( "") }
    scope = rememberCoroutineScope()
    navController = nav

    if (firstRun)
    {
        firstRun = false
        initializeCalendarMainScreen()
        MyViewModelProvider.taskModel.getByDate("")
    }


    Scaffold(
        modifier = Modifier
            .background(CustomThemeManager.colors.bgMain),
        scaffoldState = scaffoldState,
        backgroundColor = Color.Transparent,
        topBar = { MainScreen_Top() },
        bottomBar = { MainScreen_Bottom() },
        drawerContent = { MainScreen_Drawer() },
        drawerGesturesEnabled = true,
        drawerShape = RectangleShape,
        content = { padding ->
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .background(CustomThemeManager.colors.bgMain)
                    .padding(padding),
                color = Color.Transparent
            )
            {
                MainScreen_Body()
            }
        },

    )
}

fun <T> SnapshotStateList<T>.swapList(newList: List<T>)
{
    if (newList.isNotEmpty())
    {
        add(0, newList[0])
        removeAt(0)
    }
}


@Composable
fun CalendarMode()
{
    val rowState = rememberLazyListState()
    LazyRow(
        modifier = Modifier
            .fillMaxWidth()
            .height(80.dp)
            .padding(8.dp),
        state = rowState
    )
    {
        items(mainInstance.days,key = {it.day}) { item ->
            CreateCalendarItem(item)
            {
                for (i in mainInstance.days)
                {
                    i.isSelected = false
                }
                item.isSelected = true
                selectedDayInMainScreen = item.day
                MyViewModelProvider.taskModel.getByDate("$selectedYear/" +
                        "${if (selectedMonth<10) "0" else ""}$selectedMonth/" +
                        "${if (selectedDayInMainScreen<10) "0" else ""}$selectedDayInMainScreen"
                        )
                mainInstance.days.swapList(mainInstance.days)
            }

        }


    }
    MyViewModelProvider.taskModel.getByDate("$selectedYear/" +
            "${if (selectedMonth<10) "0" else ""}$selectedMonth/" +
            "${if (selectedDayInMainScreen<10) "0" else ""}$selectedDayInMainScreen"
    )
    scope.launch {
        rowState.animateScrollToItem(selectedDayInMainScreen-1)
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ToDoItem()
{
    if (mainInstance.taskStateList.isEmpty())
    {
        return
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth(),
        state = rememberLazyListState()
    )
    {

        if (doneMode.value)
        {
            items(mainInstance.taskStateList, key = {it.id}){ item ->
                CreateToDoItem(task = item, type = TaskType.DONE, imageRes = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.imageRes,
                    bgColor = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.bgColor)
                {
                    item.selected = if(item.selected == 0) 1 else 0
                    mainInstance.taskStateList.swapList(mainInstance.taskStateList)
                }
            }
        }
        else if (calenderMode.value)
        {
            items(mainInstance.taskStateList, key = {it.id}){ item ->
                CreateToDoItem(item,TaskType.CALENDAR,imageRes = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.imageRes,
                    bgColor = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.bgColor)
            }
        }
        else
        {
            items(mainInstance.taskStateList, key = {it.id}) { item ->
                val dismissState = rememberDismissState()
                if (dismissState.currentValue != DismissValue.Default) {
                    LaunchedEffect(Unit) {
                        dismissState.reset()
                    }
                    scope.launch(Dispatchers.IO){
                        MyViewModelProvider.taskModel.delete(item)
                    }
                }



                SwipeToDismiss(
                    state = dismissState,
                    modifier = Modifier
                        .padding(vertical = 0.dp),
                    directions = setOf(
                        DismissDirection.EndToStart
                    ),
                    dismissThresholds = { direction ->
                        FractionalThreshold(if (direction == DismissDirection.EndToStart) 0.1f else 0.05f)
                    },
                    background = {
                        val color by animateColorAsState(
                            when (dismissState.targetValue) {
                                DismissValue.Default -> Color.Transparent
                                else -> CustomThemeManager.colors.swipeColor
                            }
                        )
                        val alignment = Alignment.CenterEnd
                        val icon = Icons.Default.Delete

                        val scale by animateFloatAsState(
                            if (dismissState.targetValue == DismissValue.Default) 0.75f else 1f
                        )

                        Box(
                            Modifier
                                .fillMaxSize()
                                .background(color)
                                .padding(horizontal = 20.dp),
                            contentAlignment = alignment
                        ) {
                            Icon(
                                icon,
                                contentDescription = "Delete Icon",
                                modifier = Modifier.scale(scale),
                                tint = Color.White
                            )
                        }
                    },
                    dismissContent = {
                        CreateToDoItem(item,TaskType.NORMAL,imageRes = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.imageRes,
                            bgColor = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.bgColor)

                    }
                )
            }
        }
    }
}

@Composable
fun MainScreen_Body()
{
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Transparent),
        contentAlignment = Alignment.Center,
    )
    {
        Column(
            modifier = Modifier
                .fillMaxSize(),
        )
        {
            if (calenderMode.value)
            {
                CalendarMode()
            }
            if (mainInstance.taskStateList.isNotEmpty())
                ToDoItem()
            else
            {
                Row(
                    modifier = Modifier
                        .fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                )
                {
                    CreateNormalText(
                        textString = stringResource(id = R.string.noItemToShow),
                        align = TextAlign.Start
                    )
                }
            }
        }

        if (showDatePicker.value)
        {
            showDatePickerDialog()
        }
        if (showChangeLanguageAlert.value)
        {
            ChangeLanguageAlert()
        }

    }


}

@Composable
fun MainScreen_Bottom()
{
    var showDoneButton by remember { mutableStateOf(false) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically
    )
    {
        if (!showDoneButton)
        {
            CreateCircleButton(buttonSize = 48, R.drawable.ic_done_list)
            {
                showDoneButton = true
                doneMode.value = true
                MyViewModelProvider.taskModel.getByDate("")
                if (calenderMode.value) calenderMode.value = false
            }
            CreateCircleButton(
                buttonSize = 64,
                imageRes = if (!calenderMode.value) R.drawable.ic_filter_list else R.drawable.ic_filter_list_opened
            )
            {
                calenderMode.value = !calenderMode.value
                if (calenderMode.value)
                {
                    titleText.value = lstMonthsName[selectedMonth-1] + selectedYear
                }
                else
                {
                    titleText.value = currentContext.getString(R.string.app_name)
                    MyViewModelProvider.taskModel.getByDate("")
                }
            }
            CreateCircleButton(buttonSize = 48, R.drawable.ic_new_item)
            {
                navController.navigate(Screen.NewTask.route)
            }
        }
        else
        {
            CreateCircleButton(buttonSize = 64, R.drawable.ic_cancell)
            {
                showDoneButton = false
                doneMode.value = false
            }
            CreateCircleButton(buttonSize = 64, R.drawable.ic_delete_item)
            {
                try
                {
                    scope.launch(Dispatchers.IO)
                    {
                        if (!MyViewModelProvider.authModel.authenticateTaskListForDone(mainInstance.taskStateList))
                        {
                            scaffoldState.snackbarHostState.showSnackbar(message = currentContext.getString(R.string.pleaseSelectItemForDone)
                                , duration = SnackbarDuration.Short)
                            return@launch
                        }
                        setTaskDone()
                        showDoneButton = false
                        doneMode.value = false
                    }

                }
                catch (e:Exception)
                {
                    e.printStackTrace()
                }

            }
        }

    }
}

@Composable
fun MainScreen_Top()
{
    TopAppBar(
        title =
        {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        if (calenderMode.value) {
                            showDatePicker.value = true
                        }
                    },
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            )
            {
                CreateNormalText(
                    textString = titleText.value,
                    align = TextAlign.Center
                )
            }

        } ,
        elevation = 0.dp,
        backgroundColor = Color.Transparent,
        //actions show in the right screen
        actions = {
            IconButton(
                onClick = {
                    scope.launch(Dispatchers.IO)
                    {
                        if (scaffoldState.drawerState.isClosed)
                            scaffoldState.drawerState.open()
                        else
                            scaffoldState.drawerState.close()
                    }
                }
            )
            {
                Image( painter = painterResource(id = R.drawable.ic_drawer_menu), contentDescription = "")
            }
        },
    )
}

@Composable
fun MainScreen_Drawer()
{
    var darkModeState by remember { mutableStateOf(App.darkModeEnabled) }
    Column(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .background(CustomThemeManager.colors.drawerColor),
        horizontalAlignment = Alignment.CenterHorizontally,
    )
    {
        Row(
            modifier = Modifier
                .width(200.dp)
                .height(200.dp)
                .padding(16.dp)
                .clip(CircleShape)
                .background(color = Color.White, shape = CircleShape),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center

        )
        {
            Image(
                painter = painterResource(id = R.drawable.ic_logo_colored),
                contentDescription = ""
            )
        }
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()

                .padding(horizontal = 16.dp) ,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        )
        {
            CreateNormalText(
                textString = stringResource(id = R.string.language),
                align = TextAlign.Start,
                fontWeight = FontWeight.Bold
            )

            CreateSpinner(modifier = Modifier
                .wrapContentSize()
                , list = currentContext.resources.getStringArray(R.array.languageSupported).toList()) {
                

                scope.launch(Dispatchers.IO){
                    if (App.appLanguage == App.ENGLISH)
                    {
                        if (it != currentContext.getString(R.string.english))
                        {
                            App.tmpString = "fa"
                            showChangeLanguageAlert.value = true
                            scaffoldState.drawerState.close()
                        }
                    }
                    else
                    {
                        if (it != currentContext.getString(R.string.persian))
                        {
                            App.tmpString = "en"
                            showChangeLanguageAlert.value = true
                            scaffoldState.drawerState.close()
                        }
                    }
                }
            }
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(20.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()

                .padding(horizontal = 16.dp) ,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        )
        {
            CreateNormalText(
                textString = stringResource(id = R.string.darkTheme),
                align = TextAlign.Start,
                fontWeight = FontWeight.Bold
            )
            Switch(
                checked = darkModeState
                , onCheckedChange = {
                    darkModeState = it
                    App.darkModeEnabled = it
                    scope.launch(Dispatchers.IO){
                        App.dataStore.saveDarkModeValue(currentContext,it)
                    }
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = CustomThemeManager.colors.bgButtonColor,
                    checkedTrackColor = CustomThemeManager.colors.bgButtonColor
                )
            )
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(20.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()

                .padding(horizontal = 16.dp) ,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        )
        {
            CreateNormalText(
                textString = stringResource(id = R.string.doneList),
                align = TextAlign.Start,
                fontWeight = FontWeight.Bold
            )

            Button(
                onClick =
                {
                    scope.launch{
                        scaffoldState.drawerState.close()
                        navController.navigate(Screen.DoneList.route)
                    }

                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = CustomThemeManager.colors.bgButtonColor
                )
            )
            {
                CreateNormalText(
                    textString = stringResource(id = R.string.show),
                    align = TextAlign.Start,
                    textColor = Color.White
                )
            }
        }


    }
}

@Composable
fun ChangeLanguageAlert()
{
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(start = 16.dp, end = 24.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(
                width = 2.dp,
                CustomThemeManager.colors.logoBorder,
                shape = RoundedCornerShape(16.dp)
            )
            .background(color = CustomThemeManager.colors.bgScreen, shape = RoundedCornerShape(16.dp)),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        Spacer(modifier = Modifier.padding(top = 16.dp))
      CreateNormalText(textString = stringResource(id = R.string.notice), align = TextAlign.Start)
        Spacer(modifier = Modifier.padding(top = 16.dp))
      CreateNormalText(textString = stringResource(id = R.string.changeLanguageAlert), align = TextAlign.Start)
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(vertical = 16.dp),
            verticalAlignment = Alignment.Bottom,
            horizontalArrangement = Arrangement.SpaceEvenly
        )
        {
            CreatePositiveButton(text = stringResource(id = R.string.yes), width = 120, height = 40)
            {
                Log.e("3691", App.tmpString )
                Log.e("3691", App.appLanguage )
                if (App.dataStore.saveLanguageValue(currentContext, App.tmpString))
                {
                    showChangeLanguageAlert.value = false
                    (currentContext as MainActivity).restartApp()
                }

            }

            CreateNegativeButton(text = stringResource(id = R.string.no), width = 120, height = 40)
            {
                showChangeLanguageAlert.value = false
            }
        }
    }
}


@Composable
fun showDatePickerDialog()
{
    val date = selectedDateState.value.split("/")
    var yearValueSate by remember { mutableStateOf(date[0].toInt()) }
    var monthValueSate by remember { mutableStateOf(date[1].toInt()) }
    selectedYear = yearValueSate
    selectedMonth = monthValueSate
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(start = 16.dp, end = 24.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(
                width = 2.dp,
                CustomThemeManager.colors.logoBorder,
                shape = RoundedCornerShape(16.dp)
            )
            .background(color =  CustomThemeManager.colors.bgScreen, shape = RoundedCornerShape(16.dp)),
    )
    {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
            ,
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            NumberPicker(
                modifier = Modifier
                    .weight(0.33f),
                value = yearValueSate,
                range = MyViewModelProvider.dateModel.getYearRange(),
                onValueChange = {
                    yearValueSate = it
                    selectedYear = it
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )
            ListItemPicker(
                modifier = Modifier
                    .weight(0.33f),
                label  = { lstMonthsName[it-1]},
                value = monthValueSate,
                onValueChange = {
                    monthValueSate = it
                    selectedMonth = it
                },
                dividersColor = CustomThemeManager.colors.dividerColor,
                list = (1..12).toList(),
                textStyle = TextStyle(
                    color = CustomThemeManager.colors.textColor,
                    fontWeight = FontWeight.Normal,
                    fontSize = 16.sp,
                    fontStyle = FontStyle.Normal,
                )
            )

        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            CreatePositiveButton(
                text = stringResource(id = R.string.submit),
                width = 120,
                height = 40
            )
            {
                MyViewModelProvider.dateModel.getDayList(selectedYear,selectedMonth)
                selectedDateState.value = "${selectedYear}/${selectedMonth}"
                titleText.value = lstMonthsName[selectedMonth-1] + selectedYear
                showDatePicker.value = false
                MyViewModelProvider.taskModel.getByDate("$selectedYear/" +
                        "${if (selectedMonth<10) "0" else ""}$selectedMonth/" +
                        "${if (selectedDayInMainScreen<10) "0" else ""}$selectedDayInMainScreen"
                )
            }
        }
    }
}

fun initializeCalendarMainScreen()
{
    scope.launch(Dispatchers.IO)
    {
        val currentDateTime = MyViewModelProvider.dateModel.getCurrentDateTime()
        MyViewModelProvider.dateModel.initializeCalendar(currentDateTime[2].toInt(),currentDateTime[3].toInt(),currentDateTime[4].toInt(),true)
        lstMonthsName.clear()
        lstMonthsName.addAll(MyViewModelProvider.dateModel.getMonthsName())
        lstMonthsName.removeAt(0)
        selectedYear = MyViewModelProvider.dateModel.getCurrentYear()
        selectedMonth = MyViewModelProvider.dateModel.getCurrentMonth()
        Log.e("3691", "Current Day Is : KKKKKKKKK" )
        //selectedDay = dateModel.getCurrentDay()
        MyViewModelProvider.dateModel.getDayList(selectedYear,selectedMonth)
        withContext(Dispatchers.Main)
        {
            selectedDateState.value = "$selectedYear/$selectedMonth"
        }

    }
}


fun setTaskDone()
{
    try
    {
        scope.launch(Dispatchers.IO)
        {
            for (item in mainInstance.taskStateList)
            {
                if (item.selected == 1)
                {
                    item.doneState = 1
                    MyViewModelProvider.taskModel.update(item)
                }
            }

            scaffoldState.snackbarHostState.showSnackbar(message = currentContext.getString(R.string.itemsSuccessFullyDone)
                , duration = SnackbarDuration.Short)
        }
    }
    catch (e:Exception)
    {
        e.printStackTrace()
    }
}

enum class TaskType
{
    NORMAL,DONE,CALENDAR
}