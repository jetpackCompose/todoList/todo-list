package ir.saeid.todo.ui.screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
@Composable
fun SetupNav(navController: NavHostController)
{
    NavHost(navController = navController, startDestination = Screen.Splash.route)
    {
        composable(route = Screen.Home.route)
        {
            MainScreen(nav = navController)
        }
        composable(route = Screen.Splash.route)
        {
            SplashScreen(navController = navController)
        }
        composable(route = Screen.NewTask.route)
        {
            NewTaskScreen(nav = navController)
        }
        composable(route = Screen.DoneList.route)
        {
            DoneListScreen(nav = navController)
        }
    }
}