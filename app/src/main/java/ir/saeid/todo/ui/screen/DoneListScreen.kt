package ir.saeid.todo.ui.screen

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ir.saeid.todo.R
import ir.saeid.todo.ui.activity.MainActivity
import ir.saeid.todo.ui.component.CreateNormalText
import ir.saeid.todo.ui.component.CreateToDoItem
import ir.saeid.todo.ui.theme.CustomThemeManager
import ir.saeid.todo.viewmodel.MyViewModelProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private lateinit var mainInstance: MainActivity
private lateinit var scope : CoroutineScope
@Composable
fun DoneListScreen(nav: NavController)
{

    scope = rememberCoroutineScope()
    mainInstance = LocalContext.current as MainActivity

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(CustomThemeManager.colors.bgMain),
        color = Color.Transparent
    )
    {
        if (mainInstance.doneTaskStateList.isNotEmpty())
            ShowDoneList()
        else
        {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            )
            {
                CreateNormalText(
                    textString = stringResource(id = R.string.noItemToShow),
                    align = TextAlign.Start
                )
            }
        }
    }
}
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ShowDoneList()
{
    LazyColumn(
        modifier = Modifier
            .fillMaxSize(),
        state = rememberLazyListState()
    )
    {
        items(mainInstance.doneTaskStateList, key = {it.id}) { item ->
            val dismissState = rememberDismissState()
            if (dismissState.currentValue != DismissValue.Default) {
                LaunchedEffect(Unit) {
                    dismissState.reset()
                }
                scope.launch(Dispatchers.IO){
                    MyViewModelProvider.taskModel.delete(item)
                }
            }



            SwipeToDismiss(
                state = dismissState,
                modifier = Modifier
                    .padding(vertical = 0.dp),
                directions = setOf(
                    DismissDirection.EndToStart
                ),
                dismissThresholds = { direction ->
                    FractionalThreshold(if (direction == DismissDirection.EndToStart) 0.1f else 0.05f)
                },
                background = {
                    val color by animateColorAsState(
                        when (dismissState.targetValue) {
                            DismissValue.Default -> Color.Transparent
                            else -> CustomThemeManager.colors.swipeColor
                        }
                    )
                    val alignment = Alignment.CenterEnd
                    val icon = Icons.Default.Delete

                    val scale by animateFloatAsState(
                        if (dismissState.targetValue == DismissValue.Default) 0.75f else 1f
                    )

                    Box(
                        Modifier
                            .fillMaxSize()
                            .background(color)
                            .padding(horizontal = 20.dp),
                        contentAlignment = alignment
                    ) {
                        Icon(
                            icon,
                            contentDescription = "Delete Icon",
                            modifier = Modifier.scale(scale),
                            tint = Color.White
                        )
                    }
                },
                dismissContent = {
                    CreateToDoItem(item,TaskType.NORMAL,imageRes = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.imageRes,
                        bgColor = MyViewModelProvider.categoryModel.getByID(item.categoryID)!!.bgColor)

                }
            )
        }
    }
}
