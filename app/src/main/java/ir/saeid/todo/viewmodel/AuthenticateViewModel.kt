package ir.saeid.todo.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.saeid.todo.data.model.entity.TaskEntity
import ir.saeid.todo.data.repository.AuthenticateRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
 class AuthenticateViewModel@Inject constructor(private val repo : AuthenticateRepository): ViewModel()
{
    fun authenticateText(data:String,minLength:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking repo.authenticateText(data,minLength)
        }
    }

    fun authenticateInt(data:Int,notValue:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking repo.authenticateInt(data,notValue)
        }
    }

    fun authenticateTaskListForDone(data:List<TaskEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO){
            return@runBlocking repo.authenticateTaskListForDone(data)
        }
    }

    fun convertToEng(input: String): String
    {
        var numbers = input
        val persian = charArrayOf('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        val arabic = charArrayOf('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩')
        val english = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        for (i in 0..9)
        {
            numbers = numbers.replace(persian[i], english[i]).replace(arabic[i], english[i])
        }
        return numbers
    }
}