package ir.saeid.todo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.saeid.todo.data.model.model.DateModel
import ir.saeid.todo.data.repository.DateRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class DateViewModel @Inject constructor(private val repo: DateRepository): ViewModel()
{
    var days = MutableLiveData<List<DateModel>>()

    fun getDayList(year: Int,month:Int)
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            days.postValue(repo.getDayList(year,month))
        }
    }

    fun getCurrentYear():Int
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getCurrentYear()}
    }

    fun getCurrentMonth():Int
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getCurrentMonth()}
    }

    fun getCurrentDay():Int
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getCurrentDay()}
    }

    fun initializeCalendar(year:Int,month:Int,day:Int,isInitializedWithDevice : Boolean)
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            repo.initializeCalendar(year,month,day,isInitializedWithDevice)
        }
    }

    fun getMonthsName():List<String>
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getMonthsName()}
    }

    fun isLeap(year:Int):Boolean
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.isLeap(year)}
    }

    fun getSplitDate():List<String>
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getSplitDate()}
    }

    fun getCurrentDateTime():List<String>
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getCurrentDateTime()}
    }

    fun getYearRange():Iterable<Int>
    {
        return runBlocking(Dispatchers.IO){return@runBlocking repo.getYearRange()}
    }
}