package ir.saeid.todo.viewmodel

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.saeid.todo.data.model.entity.TaskEntity
import ir.saeid.todo.data.repository.TaskRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class TaskViewModel @Inject constructor(private val repo: TaskRepository): ViewModel()
{
    private val query = MutableStateFlow("")
    lateinit var tasks : Flow<List<TaskEntity>>

    init {
        viewModelScope.launch {
            tasks = query.flatMapLatest {
                if (it.isEmpty())
                    repo.getAllData()
                else
                    repo.getByDate(it)
            }

        }

    }

     private fun getAllData(date : String = "")
     {
         query.value = date
     }





    fun getAllTaskIsDone():Flow<List<TaskEntity>> = repo.getAllTaskIsDone()

    fun getByDate(date:String) = getAllData(date)

    fun update(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.update(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insert(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.insert(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insertList(data: List<TaskEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.insertList(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun delete(data: TaskEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.delete(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun deleteAll():Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.deleteAll()
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }
}