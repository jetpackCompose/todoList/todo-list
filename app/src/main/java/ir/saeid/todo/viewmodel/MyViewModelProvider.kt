package ir.saeid.todo.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import ir.saeid.todo.ui.activity.MainActivity
object MyViewModelProvider
{
    lateinit var taskModel :TaskViewModel
    lateinit var dateModel :DateViewModel
    lateinit var categoryModel :CategoryViewModel
    lateinit var authModel :AuthenticateViewModel
    fun initial(context: Context)
    {
        taskModel = ViewModelProvider(context as MainActivity)[TaskViewModel::class.java]
        dateModel = ViewModelProvider(context)[DateViewModel::class.java]
        categoryModel = ViewModelProvider(context)[CategoryViewModel::class.java]
        authModel = ViewModelProvider(context)[AuthenticateViewModel::class.java]
    }

}