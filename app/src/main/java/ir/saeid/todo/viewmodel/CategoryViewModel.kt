package ir.saeid.todo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.saeid.todo.data.model.entity.CategoryEntity
import ir.saeid.todo.data.repository.CategoryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: CategoryRepository): ViewModel()
{
    fun getAllData(): LiveData<List<CategoryEntity>>
    {
        return runBlocking(Dispatchers.IO)
        {
            return@runBlocking repo.getAllData()
        }
    }

    fun getByID(id:Int): CategoryEntity?
    {
        return runBlocking(Dispatchers.IO)
        {
            return@runBlocking repo.getByID(id)
        }
    }

    fun update(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.update(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insert(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.insert(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun insertList(data: List<CategoryEntity>):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.insertList(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun delete(data: CategoryEntity):Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.delete(data)
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }

    fun deleteAll():Boolean
    {
        return runBlocking(Dispatchers.IO)
        {
            try
            {
                repo.deleteAll()
                return@runBlocking true
            }
            catch (e:Exception)
            {
                e.printStackTrace()
                return@runBlocking false
            }
        }
    }
}