package ir.saeid.todo

import android.app.Application
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import dagger.hilt.android.HiltAndroidApp
import ir.saeid.todo.tool.DataStoreTool
import ir.saeid.todo.ui.theme.CustomTheme
import ir.saeid.todo.ui.theme.CustomThemeManager


@HiltAndroidApp
class App : Application()
{

    companion object{
        lateinit var  dataStore : DataStoreTool
        const val CATEGORY_COUNT = 6
        const val MIN_TEXT_LENGTH = 5
        const val SPLASH_SCREEN_TIME = 2000L
        const val ENGLISH = "en"
        const val PERSIAN = "fa"
        const val PREFERENCE_NAME = "TODO"
        var appLanguage = ""
        var tmpString = ""
        var darkModeEnabled = false

        val LANGUAGE_DATA_KEY = stringPreferencesKey("Language")
        val DARK_MODE_DATA_KEY = booleanPreferencesKey("DarkMode")

    }

    override fun onCreate() {
        super.onCreate()
        dataStore = DataStoreTool()
        appLanguage = dataStore.getLanguageValue(this)
        if (dataStore.getDarkModeValue(this))
        {
            CustomThemeManager.customTheme = CustomTheme.DARK
            darkModeEnabled = true
        }
        else
        {
            CustomThemeManager.customTheme = CustomTheme.LIGHT
            darkModeEnabled = false
        }

    }
}